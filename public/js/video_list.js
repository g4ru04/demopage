var authToken = "key";

$(function(){

    //Date Picker set
    $("#fromTime").datepicker("setDate", new Date("2019/03/01"));
    $("#tillTime").datepicker("setDate", new Date());

    //init VideoListTable parameter to control table
    initVideoListTable();
    let first_time = true;
    queryVideoListData(first_time);

    //Belows are event handler * 5
    // Search Data
    $("#search").click(function(){
        $(".query-section").addClass("init");
        VideoListTable.setMessageTable("資料讀取中，請稍後");
        VideoListTable.pageNumber = 0;
        queryVideoListData();
    });

    // press button to play video
    $(".query-table").delegate(".btn-play","click",function(){

        $(".query-section").removeClass("init");

        $(".playing").text("");
        $(".playing").addClass("videoplay");
        $(".playing").removeClass("playing");

        $(this).text("播放中");
        $(this).addClass("playing");
        $(this).removeClass("videoplay");
        $(".query-section").addClass("force-fold");
        setTimeout(function(){
            $(".query-section").removeClass("force-fold");
        },600);

        let title = $(this).attr("video_title");
        let note = $(this).attr("note");
        setVideo( title, note,$(this).attr("source"));

    });

    // Change page
    $("#next_page").click(function(){
        VideoListTable.getNextPage();
    });
    $("#prev_page").click(function(){
        VideoListTable.getPrevPage();
    });
    $("#page_n").keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            VideoListTable.pageNumber= +$(this).val()-1;
            queryVideoListData();
        }
     });

    $("form").delegate("input","focus",function(){
        $(".query-section").addClass("init");
    });

    // video tag error
    $("#video").on('error', function(event) {
        warning("若無法播放視訊，請重新登入。");
    });
	$(".video-wrap").click(function(){
		$(".query-section").removeClass("init");
		$(".query-section").addClass("force-fold");
        setTimeout(function(){
            $(".query-section").removeClass("force-fold");
        },600);
	});

    // close error message
    $(".sys-error-msg").delegate(".btn-msg-close","click",function(){
        $(this).parent($('.sys-error-msg-inner')).fadeOut(200);
    });

});

// fetch data from AP server
function queryVideoListData(first_time,callback){

    setVideo("","",null);
	if(VideoListTable.pageNumber==0){
		return_data = {"pageNumber":0,"pageLimit":10,"totalAmount":11,"data":[{"callUuid":"c1df0597b7194ed7a6f93e5409b480fb","eventStatus":"{2,3,4,7}","userId":"{A123456789,44660}","systemId":"pershing","startTime":"2019/03/03 13:13:18","eventTime":"00:00:15","fileName":"pershing-c1df0597b7194ed7a6f93e5409b480fb.mp4","urlData":"/video/files/pershing-c1df0597b7194ed7a6f93e5409b480fb.mp4","status":"1"},{"callUuid":"d44f7e3e56e247b688a6b34cc9a5cc59","eventStatus":"{2,3,4,9}","userId":"{H288333279,44660}","systemId":"pershing","startTime":"2019/03/04 13:14:59","eventTime":"00:00:00","fileName":"pershing-d44f7e3e56e247b688a6b34cc9a5cc59.mp4","urlData":"/video/files/pershing-d44f7e3e56e247b688a6b34cc9a5cc59.mp4","status":"2"},{"callUuid":"b97a3658473d4579a708e06574161a55","eventStatus":"{2,3,4}","userId":"{A126395764,44660}","systemId":"pershing","startTime":"2019/03/05 13:14:59","eventTime":"00:00:00","fileName":"pershing-b97a3658473d4579a708e06574161a55.mp4","urlData":"/video/files/pershing-b97a3658473d4579a708e06574161a55.mp4","status":"0"},{"callUuid":"9aa4f05bd89c4354a6810dae1f769aff","eventStatus":"{20}","userId":"{F148346556,44280}","systemId":"pershing","startTime":"2019/03/06 13:14:59","eventTime":"00:00:00","fileName":"pershing-9aa4f05bd89c4354a6810dae1f769aff.mp4","urlData":"/video/files/pershing-9aa4f05bd89c4354a6810dae1f769aff.mp4","status":"1"},{"callUuid":"89e5d0c70c484a928efd762795d77298","eventStatus":"{2,3,4,7}","userId":"{A226335764,44660}","systemId":"pershing","startTime":"2019/03/13 19:59:17","eventTime":"00:00:15","fileName":"pershing-89e5d0c70c484a928efd762795d77298.mp4","urlData":"/video/files/pershing-89e5d0c70c484a928efd762795d77298.mp4","status":"2"},{"callUuid":"27dc3c17deab402d9cd6bdd52f823460","eventStatus":"{2,3,4,9}","userId":"{H288333279,44280}","systemId":"pershing","startTime":"2019/03/13 20:00:01","eventTime":"00:00:00","fileName":"pershing-27dc3c17deab402d9cd6bdd52f823460.mp4","urlData":"/video/files/pershing-27dc3c17deab402d9cd6bdd52f823460.mp4","status":"0"},{"callUuid":"b406ff830ab34d05bb9dc5eb914ecd10","eventStatus":"{2,3,4}","userId":"{F148346556,44280}","systemId":"pershing","startTime":"2019/03/13 20:00:28","eventTime":"00:00:00","fileName":"pershing-b406ff830ab34d05bb9dc5eb914ecd10.mp4","urlData":"/video/files/pershing-b406ff830ab34d05bb9dc5eb914ecd10.mp4","status":"1"},{"callUuid":"fc1918d5f65e401194c8947ad858953d","eventStatus":"{20}","userId":"{F126821676,44660}","systemId":"pershing","startTime":"2019/03/13 20:00:51","eventTime":"00:00:00","fileName":"pershing-fc1918d5f65e401194c8947ad858953d.mp4","urlData":"/video/files/pershing-fc1918d5f65e401194c8947ad858953d.mp4","status":"2"},{"callUuid":"3cde297532c8473f916bc7c632640fe3","eventStatus":"{2,3,4,7}","userId":"{A226335764,44660}","systemId":"pershing","startTime":"2019/03/13 21:19:31","eventTime":"00:00:15","fileName":"pershing-3cde297532c8473f916bc7c632640fe3.mp4","urlData":"/video/files/pershing-3cde297532c8473f916bc7c632640fe3.mp4","status":"0"},{"callUuid":"9cab630754c2465d96780772dd183aa4","eventStatus":"{2,3,4,7}","userId":"{F126821676,44280}","systemId":"pershing","startTime":"2019/03/13 21:20:11","eventTime":"00:21:26","fileName":"pershing-9cab630754c2465d96780772dd183aa4.mp4","urlData":"/video/files/pershing-9cab630754c2465d96780772dd183aa4.mp4","status":"1"}],"returnCode":"S001","serverTime":"2019/03/18-11:37:32"};
	}else{
		return_data = {"pageNumber":1,"pageLimit":10,"totalAmount":11,"data":[{"callUuid":"e4fd82bc6c1d4098b2d61d42844b439a","eventStatus":"{2,3,4,7}","userId":"{A226335764,44660}","systemId":"pershing","startTime":"2019/03/13 21:21:07","eventTime":"00:00:15","fileName":"pershing-e4fd82bc6c1d4098b2d61d42844b439a.mp4","urlData":"/video/files/pershing-e4fd82bc6c1d4098b2d61d42844b439a.mp4","status":"2"}],"returnCode":"S001","serverTime":"2019/03/18-11:39:26"};
	}
/*
    $.ajax({
        type : "POST",
        url : "/api/videoList",
        cache : false,
        headers: {
            'Content-Type': 'application/json'
        },
        dataType: "json",
        data : JSON.stringify({
            startTime: $("#fromTime").val(),
            endTime: $("#tillTime").val(),
            systemId: $("#sysName").val(),
            apiKey: authToken,
            csAcc: $("#csAcc").val(),
            clientAcc: $("#clientAcc").val(),
            pageLimit: VideoListTable.pageLimit,
            pageNumber: VideoListTable.pageNumber
        }),
        success: function(return_data) {
			*/
			
            console.log(return_data);
            if(return_data.returnCode!="S001"|| !Array.isArray(return_data.data)){
                warning("影片資料存取異常，請稍後再試。");

            }else{
                VideoListTable.dataAssignment(return_data);
                VideoListTable.drawTable(first_time);

                if(callback){
                    callback(return_data);
                }
            }
		/*
        },

        error: function(jqXHR, textStatus, errorThrown) {

            warning("讀取資料失敗，請重新登入。");
            VideoListTable.setMessageTable("讀取影片資料失敗");

            $(".videolist-control").hide();
            console.log(textStatus, errorThrown);

        }
    });
	*/
}

function setVideo(title,note,location){

    $(".video-title").html(title);
    $(".video-note").html(note);

    if(location != null){
		if(title.indexOf("10")!=-1){
			$("#video").attr("src","https://www.dropbox.com/s/75yc7qu31kyqj8q/demo-video.mp4?dl=0&raw=1");
		}else{
			$("#video").attr("src","https://www.dropbox.com/s/vesixd74l9z3zp9/demo-video-short.mp4?dl=0&raw=1");
		}
        $("#video").attr("controls","controls");
    }else{

        $("#video").removeAttr("src");
        $("#video").removeAttr("controls");
        $("#video").load();
    }

}


function warning(msg){
    if(!window.ErrorDisplay){
        ErrorDisplay={};
    }

    if(ErrorDisplay[msg]!=null){
        clearTimeout(ErrorDisplay[msg].timeout);
        ErrorDisplay[msg].timeout = setTimeout(function(){
            ErrorDisplay[msg].element.fadeOut(200);
            delete ErrorDisplay[msg];
        },3000);
    }else{
        let error_msg_element = $("<div/>",{
            "class":"sys-error-msg-inner",
            "html":"<span>" + msg + "</span><a class='btn-msg-close' href='#'>x</a>"
        });

        $(".sys-error-msg").append(error_msg_element);

        let timeout = setTimeout(function(){
            error_msg_element.fadeOut(200);
            delete ErrorDisplay[msg];
        },3000);

        ErrorDisplay[msg]={
            timeout:timeout,
            element:error_msg_element
        }
    }

}


function nullToStr(object) {
    return object == null ? "" : object.toString();
}

function initVideoListTable(){

    VideoListTable = {
        pageNumber: 0,
        pageLimit: 10,
        totalAmount: 0,
        data: [],
        // Set data of table element
        dataAssignment: function( response ){

            let json_data = response.data.map(function(item){
                                let customer_id = nullToStr(item.userId).replace("{","").split(",")[0];
                                let service_id = nullToStr(item.userId).replace("}","").split(",")[1];
                                return {
                                    "ID": nullToStr(item.callUuid),
                                    "SYSTEM": nullToStr(item.systemId),
                                    "SERVICE": nullToStr(service_id),
                                    "CUSTOMER": nullToStr(customer_id),
                                    "Time": nullToStr(item.startTime),
                                    "Length": nullToStr(item.eventTime),
                                    "eventStatus": nullToStr(item.eventStatus.replace(/{/g,"").replace(/}/g,"")),
                                    "Location": nullToStr(item.urlData)
                                };
                            });

            this.pageNumber = response.pageNumber;
            this.pageLimit = response.pageLimit;
            this.totalAmount = response.totalAmount;
            this.data = json_data;

        },
        // As public method to draw table
        drawTable: function(first_time){

            if( this.data.length==0 ){
                //while error response or zero response
                if(first_time){
                    this.setMessageTable("本日尚無視訊紀錄");
                }else{
                    this.setMessageTable("沒有資料喔!");
                }

            }else{
                this.setTableInfo();
                this.drawTableImpl();
            }

        },
        // Private method for drawTable
        setTableInfo: function(){

            let page_amount =  Math.floor(
                                   Math.abs(this.totalAmount - 1) / this.pageLimit + 1
                               );
            $("#page_total").html(page_amount);
            $("#page_n").val(this.pageNumber+1);

            if(this.pageNumber!=0){
                $("#prev_page").removeClass("disabled");
            }
            if(this.pageNumber!=page_amount-1){
                $("#next_page").removeClass("disabled");
            }
        },
        // Private method for drawTable
        drawTableImpl: function(){
            //Two step to draw it
            let table_ptr = this;
            //Step1. Format the data.
            let dataToDraw = this.data.map(function(item,i){

                //pre setting for display parameter
                item.idx = table_ptr.pageNumber * table_ptr.pageLimit + i+1;
                let title = item.idx+". "+item.SYSTEM+"視訊紀錄";
                let note = item.CUSTOMER + " <=> " + item.SERVICE;

                //set play video button

                let can_play = "";
                let display_word = "";

                let status_list = item.eventStatus.split(",");

                if(status_list.includes("7")){
                    can_play = "videoplay";
                    display_word = "";
                }else if(status_list.includes("9")){
                    can_play = "disabled";
                    display_word = "轉檔中";
                }else if(status_list.includes("4")){
                    can_play = "disabled";
                    display_word = "已掛斷";
                }else if(status_list.includes("20")){
                    can_play = "disabled";
                    display_word = "通話中";
                }else{
                    can_play = "disabled";
                    display_word = "異常";
                }

                item.play_btn =
                    "<a class='query-list-btn "+can_play+" btn-play' href='javascript:void(0);' "
                        + " video_title='" + title + "' note='" + note + "' source='" + item.Location + "'>"
                        + display_word+"</a>"

                return item;
            });

            //Step3. Display as html
            let tableHtmlStr = "";
            for( let i=0 ; i<dataToDraw.length ; i++ ){

                let item = dataToDraw[i];
                tableHtmlStr += "<tr>"
                    + "<td class='txt-center txt-bold'>" + item["idx"] + "</td>"
                    + "<td>" + item["SYSTEM"] + "</td>"
                    + "<td>" + item["SERVICE"] + "</td>"
                    + "<td>" + item["CUSTOMER"] + "</td>"
                    + "<td>" + item["Time"] + "</td>"
                    + "<td class='txt-center'>" + item["Length"] + "</td>"
                    + "<td class='txt-center'>" + item["play_btn"] + "</td>"
                + "</tr>" ;
            }

            $(".query-table tfoot").empty();
            $(".query-table tbody").html(tableHtmlStr);
            $(".videolist-control").show();

        },
        // As public method to show things except for data
        setMessageTable: function(msg){

            $(".query-table tbody").empty();
            $(".query-table tfoot").html("<tr><td colspan='7'><div class='query-list-empty'>"+ msg +"</div></td></tr>");
        },
        // Action for table operation
        getNextPage: function(){
            $("#next_page").addClass("disabled");
            this.pageNumber++;
            queryVideoListData();
        },
        // Action for table operation
        getPrevPage: function(){
            $("#prev_page").addClass("disabled");
            this.pageNumber--;
            queryVideoListData();
        }
    }

}
